import uglify from "rollup-plugin-uglify";
import babel from "rollup-plugin-babel";

import postcss from "rollup-plugin-postcss";
import postcssModules from "postcss-modules";

//const cssExportMap = {}

const config = {
  input: "src/DazpSlider.js",
  external: ["react"],
  plugins: [
    babel({
      exclude: "node_modules/**"
    }),
    uglify()
  ],
  output: {
    format: "umd",
    name: "DazpSlider",
    globals: {
      react: "React"
    }
  }
};
export default config;
