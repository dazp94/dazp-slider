import React from "react";
import { Carousel } from "react-responsive-carousel";
import Controller from "./Controller";
import Image from "./Image";

export default class CarouselSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = { selectedItem: 0 };
  }

  renderButton(index) {
    return (
      <div onClick={() => this.setState({ selectedItem: index })}>
        <Controller index={index} selectedItem={this.state.selectedItem} />
      </div>
    );
  }

  renderCarousel() {
    return (
      <Carousel
        onChange={e => this.setState({ selectedItem: e })}
        autoPlay
        showArrows={false}
        showThumbs={false}
        showIndicators={false}
        showStatus={false}
        infiniteLoop
        interval={this.props.interval}
        stopOnHover
        axis={"vertical"}
        selectedItem={this.state.selectedItem}
      >
        {this.createImages(this.props.payload)}
      </Carousel>
    );
  }

  renderControllers() {
    let controllers = [];
    {
      for (var i = 0; i < this.props.payload.length; i++) {
        controllers.push(this.renderButton(i));
      }
    }
    return (
      <div style={{ position: "absolute", marginTop: 20, marginLeft: 5 }}>
        {controllers}
      </div>
    );
  }

  render() {
    return (
      <div>
        <div style={{ maxWidth: 0, position: "relative", zIndex: 10 }}>
          {this.renderControllers(this.props.payload)}
        </div>
        {this.renderCarousel()}
      </div>
    );
  }

  createImages(payload) {
    let images = [];
    const array = payload;
    for (var i = 0; i < array.length; i++) {
      images.push(
        <Image
          image={array[i].image}
          width={window.innerWidth}
          height={window.innerHeight}
        />
      );
    }

    return images;
  }
}
