import React from "react";

const Image = ({ image }) => (
  <div>
    <img height={"100%"} width={"100%"} src={image} />
  </div>
);

export default Image;
