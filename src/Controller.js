import React from "react";

export default class Controller extends React.Component {
  isClicked() {
    return this.props.index == this.props.selectedItem ? true : false;
  }
  render() {
    return (
      <div
        style={{
          width: 8,
          height: 8,
          backgroundColor: this.isClicked() ? "#32325d" : "#FFFFFF",
          margin: 5,
          borderRadius: "50%"
        }}
      />
    );
  }
}
