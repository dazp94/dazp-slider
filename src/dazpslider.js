import React from "react";
import CarouselSlider from "./CarouselSlider";
import "react-responsive-carousel/lib/styles/carousel.min.css";

export default class DazpSlider extends React.Component {
  render() {
    return (
      <CarouselSlider
        payload={this.props.payload}
        interval={this.props.interval}
      />
    );
  }
}
