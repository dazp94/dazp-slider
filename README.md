**USAGE**

Create a payload with all your images and also set an interval from transitions

import React from "react";  
import ReactDOM from "react-dom";  
import DazpSlider from "dazp-carousel";  

const interval = 3000  

const payload = [
  {
    image:
      "http://bit.do/dazpCarousel_1"   },
  {
    image:
      "http://bit.do/dazpCarousel_2"  }
];  


ReactDOM.render(
  <DazpSlider payload={payload} interval={interval} />,
  document.getElementById("root")
);
